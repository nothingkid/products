import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Products from './Products';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columnDefs: [
        {headerName: "ID", field: "id"},
        {headerName: "Name", field: "name"},
        {headerName: "Price", field: "price"}
      ],
      defaultColDef: { editable: true }
    }

    this.initializeRows = this.initializeRows.bind(this);
    this.addNewRow = this.addNewRow.bind(this);
    this.removeLastRow = this.removeLastRow.bind(this);
  }

  initializeRows(rowData) {
    this.setState({rowData});
  }

  addNewRow() {
    //evaluating new ID by taking last element in an array, taking its id and
    //adding '1' to it. Then convert this value into string.
    var newId = (+this.state.rowData[this.state.rowData.length - 1]["id"] + 1) + '';
    var newRow = {
      "id": newId,
      "name": "",
      "price": ""
    }

    var rowData = Object.assign([], this.state.rowData);
    rowData.push(newRow);
    this.setState({rowData});
  }

  removeLastRow() {
    var rowData = Object.assign([], this.state.rowData);
    rowData.pop();
    this.setState({rowData});
  }

  render() {
    return (
      <div className="App">
        <Products
          columnDefs={this.state.columnDefs}
          rowData={this.state.rowData}
          defaultColDef={this.state.defaultColDef}

          initializeRows={this.initializeRows}
          addNewRow={this.addNewRow}
          removeLastRow={this.removeLastRow}
        >
        </Products>
      </div>
    );
  }
}

export default App;
