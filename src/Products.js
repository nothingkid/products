import React, { Component } from 'react';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-balham.css';

import rowData from './data.json';

class Products extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.initializeRows(rowData);
  }

  render() {
    return (
      <div
        className="Products ag-theme-balham"
        style={{
	         height: '500px',
	         width: '600px'
        }}
        >
        <AgGridReact
          enableSorting={true}
          enableFilter={true}
          pagination={true}
          columnDefs={this.props.columnDefs}
          rowData={this.props.rowData}
          defaultColDef={this.props.defaultColDef}>
        </AgGridReact>
        <button className="addNewRow" onClick={this.props.addNewRow}>
          Add New Row
        </button>
        <button className="removeLastRow" onClick={this.props.removeLastRow}>
          Remove Last Row
        </button>
      </div>
    );
  }
}

export default Products;
